/* 
 path: api/peliculas
 */
const {
    Router
} = require('express');
const {
    check
} = require('express-validator');

const {
    validarCampos
} = require('../middlewares/validar-campos');
const {
    validarJWT,
    validarYearAndTitle
} = require('../middlewares/validar-jwt');

const {
    buscarPeliculas,
    obtenerPeliculas,
    searchAndReplace
} = require('../controllers/peliculas');

const router = Router();

//validarJWT
router.get('/buscador/:title', [validarJWT, validarYearAndTitle], buscarPeliculas);

router.get('/', validarJWT, obtenerPeliculas);

router.post('/searchAndReplace', [
    validarJWT,
    check('movie', 'El parametro movie es obligatorio').not().isEmpty(),
    check('find', 'El parametro find es obligatorio').not().isEmpty(),
    check('replace', 'El parametro replace es obligatorio').not().isEmpty(),
    validarCampos
], searchAndReplace);


module.exports = router;