const Koa = require('koa');
const serve = require('koa-static');
const express = require('express');
const path = require('path');
const app = new Koa();

const router = require('koa-router')();
const mount = require('koa-mount');

require('dotenv').config();





// DB Config
require('./database/config').dbConnection();


const server = require('http').createServer(app);
module.exports.io = require('socket.io')(server);
require('./sockets/socket');
// Path público
const publicPath = path.resolve(__dirname, 'public');
app.use(serve(publicPath));


// mount the route
app.use(mount(require('./routes/auth')));
app.use(mount(require('./routes/peliculas')));
app.use(router.routes()); // route middleware




app.listen(process.env.PORT, (err) => {

    if (err) throw new Error(err);

    console.log('Servidor corriendo en puerto', process.env.PORT);

});